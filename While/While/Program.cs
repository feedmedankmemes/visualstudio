﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace While
{
    class Program
    {
        static void Main(string[] args)
        {
            int lyg = 0;
            int nelyg = 0;
            Console.WriteLine("ivesk teigiama skaiciu");
            int num = int.Parse(Console.ReadLine());
            while (num > 0)
            {
                
                if (num % 2 == 0)
                {
                    lyg += 1;
                }
                
                else 
                {
                    nelyg += 1;
                }
                num = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("lyg - " + lyg + " nelyg - " + nelyg);
            Console.ReadLine();
            
        }
    }
}
